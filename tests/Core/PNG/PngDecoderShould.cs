using ConsoleEngine.Core.Png;

namespace tests;

public class PNGDecoderShould
{
    [Fact]
    public void NoFilterScanlineCorrectly()
    {
        var length = 10;
        var random = new Random();
        var input = new int[length];
        for (var i = 0; i < length; i++)
        {
            input[i] = random.Next() % 256;
        }

        var unfiltered = new int[length];
        PNGDecoder.NoFilterScanLine(input, 0, unfiltered);
        for (var i = 0; i < length; i++)
        {
            Assert.Equal(input[i], unfiltered[i]);
        }
    }

    [Fact]
    public void SubFilterScanline1ByteLookbackCorrectly()
    {
        var input = new int[] { 0, 0, 0, 0, 100, 0, 0, 50, 0 };
        var expected = new int[] { 0, 0, 0, 0, 100, 100, 100, 150, 150 };

        var unfiltered = new int[input.Length];
        PNGDecoder.SubFilterScanLine(input, 0, unfiltered, 1);
        for (var i = 0; i < input.Length; i++)
        {
            Assert.Equal(expected[i], unfiltered[i]);
        }
    }

    [Fact]
    public void SubFilterScanline2ByteLookbackCorrectly()
    {
        var input = new int[] { 0, 0, 0, 0, 100, 0, 0, 50, 0 };
        var expected = new int[] { 0, 0, 0, 0, 100, 0, 100, 50, 100 };

        var unfiltered = new int[input.Length];
        PNGDecoder.SubFilterScanLine(input, 0, unfiltered, 2);
        for (var i = 0; i < input.Length; i++)
        {
            Assert.Equal(expected[i], unfiltered[i]);
        }
    }

    [Fact]
    public void UpFilterScanlineCorrectly()
    {
        var input = new int[] { 0, 0, 0, 0, 100, 0, 0, 50 };

        var unfiltered = new int[]{
            10, 25, 0, 0, 30, 0, 10, 10,
            0, 0, 0, 0, 0, 0, 0, 0
        };
        var expected = new int[] { 10, 25, 0, 0, 130, 0, 10, 60 };
        PNGDecoder.UpFilterScanLine(input, 1, unfiltered);
        for (var i = 0; i < input.Length; i++)
        {
            Assert.Equal(expected[i], unfiltered[input.Length + i]);
        }
    }

    [Fact]
    public void AverageFilterScanline1ByteLookbackCorrectly()
    {
        var input = new int[] { 0, 0, 0, 0, 100, 0, 0, 50 };

        var unfiltered = new int[]{
            8, 24, 0, 0, 30, 0, 10, 10,
            0, 0, 0, 0, 0, 0, 0, 0
        };

        var expected = new int[] { 4, 14, 7, 3, 116, 58, 34, 72 };
        PNGDecoder.AverageFilterScanLine(input, 1, unfiltered, 1);
        for (var i = 0; i < input.Length; i++)
        {
            Assert.Equal(expected[i], unfiltered[input.Length + i]);
        }
    }

    [Fact]
    public void PaethFilterScanlineCorrectly()
    {
        var input = new int[] { 0, 0, 0, 0, 100, 0, 0, 50 };

        var unfiltered = new int[]{
            8, 24, 0, 0, 30, 0, 10, 10,
            0, 0, 0, 0, 0, 0, 0, 0
        };

        var expected = new int[] { 8, 24, 0, 0, 130, 130, 130, 180 };
        PNGDecoder.PaethFilterScanLine(input, 1, unfiltered, 1);
        for (var i = 0; i < input.Length; i++)
        {
            Assert.Equal(expected[i], unfiltered[input.Length + i]);
        }
    }
}