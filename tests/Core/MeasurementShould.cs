using ConsoleEngine.Core;

namespace tests;

public class MeasurementShould
{
    [Fact]
    public void GetsAbsoluteValueAsExpected()
    {
        var random = new Random();
        var measurement = new Measurement()
        {
            Value = random.Next(1, 100),
            Type = Measurement.MeasurementType.Absolute
        };

        var parentValue = random.Next(1, 100);
        var abs = measurement.GetAbsoluteValue(parentValue);
        Assert.Equal(measurement.Value, abs);
    }

    [Fact]
    public void GetsPercentValueAsExpected()
    {

        var random = new Random();
        var measurement = new Measurement()
        {
            Value = random.Next(1, 100),
            Type = Measurement.MeasurementType.Percent
        };

        var parentValue = random.Next(1, 100);
        var expected = measurement.Value / 100f * parentValue;
        var abs = measurement.GetAbsoluteValue(parentValue);
        Assert.NotStrictEqual(expected, abs);
    }
}