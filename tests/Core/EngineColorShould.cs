using ConsoleEngine.Core;

namespace tests;

public class EngineColorShould
{
    [Fact]
    public void UseOriginalColorIfAddingTransparentColor()
    {
        var origColor = GetBaseColor();
        var transparent = new EngineColor() { R = 0, G = 255, B = 0, A = 0 };
        var newColor = origColor + transparent;
        Assert.Equal(origColor, newColor);
    }

    [Fact]
    public void UseNewColorIfAddingOpaqueColor()
    {
        var origColor = GetBaseColor();
        var opaque = new EngineColor() { R = 255, G = 0, B = 0, A = 255 };
        var newColor = origColor + opaque;
        Assert.Equal(opaque, newColor);
    }

    [Fact]
    public void BlendsColorsAsExpected()
    {
        var origColor = GetBaseColor();
        var blend = new EngineColor() { R = 255, G = 0, B = 0, A = 100 };
        var newColor = origColor + blend;
        var blendPortion = blend.A / 255f;
        var origPortion = 1 - blendPortion;
        var expected = new EngineColor()
        {
            R = (int)(origPortion * origColor.R) + (int)(blendPortion * blend.R),
            G = (int)(origPortion * origColor.G) + (int)(blendPortion * blend.G),
            B = (int)(origPortion * origColor.B) + (int)(blendPortion * blend.B),
            A = 255
        };
        Assert.Equal(expected, newColor);
    }

    public static EngineColor GetBaseColor() => new() { R = 100, G = 100, B = 100, A = 255 };
}