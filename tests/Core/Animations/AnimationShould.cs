using System.Linq.Expressions;
using ConsoleEngine.Core;
using ConsoleEngine.Core.Animations;
using ConsoleEngine.RenderBlocks;

namespace tests;

public class AnimationShould
{

    [Fact]
    public void UpdateFrameAsExpected()
    {
        var block = CreateFillBlock();
        var animation = CreateBasicAnimation(block);

        Assert.Equal('a', block.FillCharacter);
        animation.Reset();
        Assert.Equal('b', block.FillCharacter);
        // not enough to trigger new frame
        animation.Update(0.05);
        Assert.Equal('b', block.FillCharacter);
        // trigger new frame
        animation.Update(0.05);
        Assert.Equal('c', block.FillCharacter);
    }

    [Fact]
    public void PauseAndResumeAsExpected()
    {
        var block = CreateFillBlock();
        var animation = CreateBasicAnimation(block);

        Assert.Equal('a', block.FillCharacter);
        animation.Reset();
        Assert.Equal('b', block.FillCharacter);
        // not enough to trigger new frame
        animation.Update(0.05);
        Assert.Equal('b', block.FillCharacter);
        // enough to trigger a new frame, but paused
        animation.Paused = true;
        animation.Update(0.05);
        Assert.Equal('b', block.FillCharacter);
        // trigger new frame
        animation.Paused = false;
        animation.Update(0.05);
        Assert.Equal('c', block.FillCharacter);
    }

    [Fact]
    public void EndAsExpected()
    {
        var block = CreateFillBlock();
        var animation = CreateBasicAnimation(block);
        animation.Reset();
        Assert.False(animation.HasFinished);
        animation.Update(1);
        Assert.True(animation.HasFinished);
        Assert.True(animation.Paused);
        Assert.Equal('c', block.FillCharacter);
    }

    [Fact]
    public void LoopAsExpected()
    {
        var block = CreateFillBlock();
        var animation = CreateBasicAnimation(block);
        animation.Looping = true;
        animation.Reset();
        Assert.Equal(0, animation.CurrentFrame);
        Assert.False(animation.HasFinished);
        animation.Update(0.11);
        Assert.Equal(1, animation.CurrentFrame);
        Assert.False(animation.HasFinished);
        animation.Update(0.11);
        Assert.Equal(0, animation.CurrentFrame);
        Assert.False(animation.HasFinished);
    }

    private static Animation CreateBasicAnimation(FillBlock block) =>
        new(new List<AnimationFrame>()
        {
            new()
            {
                FrameDuration = 0.1,
                Nodes = new List<AnimationNode>()
                {
                    new () { Behavior = () => block.FillCharacter = 'b' }
                }
            },
            new()
            {
                FrameDuration = 0.1,
                Nodes = new List<AnimationNode>()
                {
                    new (){ Behavior = () => block.FillCharacter = 'c' }
                }
            },

        });

    private static FillBlock CreateFillBlock() => new()
    {
        FillCharacter = 'a',
        Top = new Measurement() { Value = 0, Type = Measurement.MeasurementType.Absolute },
        Left = new Measurement() { Value = 0, Type = Measurement.MeasurementType.Absolute },
        Width = new Measurement() { Value = 1, Type = Measurement.MeasurementType.Absolute },
        Height = new Measurement() { Value = 1, Type = Measurement.MeasurementType.Absolute },
    };
}