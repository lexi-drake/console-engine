using ConsoleEngine.Core;
using ConsoleEngine.Core.Animations;
using ConsoleEngine.RenderBlocks;

namespace tests;

public class AnimationSetShould
{
    private AnimationSet _animationSet = new();
    private FillBlock _block = new()
    {
        FillCharacter = 'a',
        Top = new Measurement() { Value = 0, Type = Measurement.MeasurementType.Absolute },
        Left = new Measurement() { Value = 0, Type = Measurement.MeasurementType.Absolute },
        Width = new Measurement() { Value = 1, Type = Measurement.MeasurementType.Absolute },
        Height = new Measurement() { Value = 1, Type = Measurement.MeasurementType.Absolute },
    };
    private string _aOne = "animation_one";
    private string _aTwo = "animation_two";

    public AnimationSetShould()
    {
        _animationSet.Animations.Add(_aOne, new(new()
        {
            new()
            {
                FrameDuration = 0.1,
                Nodes = new List<AnimationNode>()
                {
                    new () { Behavior = () => _block.FillCharacter = 'b' }
                }
            },
            new()
            {
                FrameDuration = 0.1,
                Nodes = new List<AnimationNode>()
                {
                    new (){ Behavior = () => _block.FillCharacter = 'c' }
                }
            },
        }));
        _animationSet.Animations.Add(_aTwo, new(new()
        {
            new()
            {
                FrameDuration = 0.1,
                Nodes = new List<AnimationNode>()
                {
                    new () { Behavior = () => _block.FillCharacter = 'd' }
                }
            },
            new()
            {
                FrameDuration = 0.1,
                Nodes = new List<AnimationNode>()
                {
                    new (){ Behavior = () => _block.FillCharacter = 'e' }
                }
            },
        }));
    }

    [Fact]
    public void PlaysCorrectAnimation()
    {
        Assert.Equal('a', _block.FillCharacter);
        _animationSet.Play(_aOne);
        Assert.Equal('b', _block.FillCharacter);
        _animationSet.Play(_aTwo);
        Assert.Equal('d', _block.FillCharacter);
    }

    [Fact]
    public void ResumesAnimation()
    {
        Assert.Equal('a', _block.FillCharacter);
        _animationSet.Play(_aOne);
        Assert.Equal('b', _block.FillCharacter);
        _animationSet.Udpate(0.1f);
        Assert.Equal('c', _block.FillCharacter);
        _animationSet.Play(_aTwo);
        Assert.Equal('d', _block.FillCharacter);
        _animationSet.Play(_aOne, false);
        Assert.Equal('c', _block.FillCharacter);
    }

    [Fact]
    public void PausesAnimation()
    {
        Assert.Equal('a', _block.FillCharacter);
        _animationSet.Play(_aOne);
        Assert.Equal('b', _block.FillCharacter);
        _animationSet.Pause();
        _animationSet.Udpate(0.1f);
        Assert.Equal('b', _block.FillCharacter);
    }

    [Fact]
    public void UsesAnimationSpeed()
    {
        Assert.Equal('a', _block.FillCharacter);
        _animationSet.AnimationSpeed = 2;
        _animationSet.Play(_aOne);
        _animationSet.Udpate(0.1f);
        Assert.Equal('c', _block.FillCharacter);
    }

    [Fact]
    public void HandlesEmptyDictionary()
    {
        var animations = new AnimationSet();
        animations.Play("hello");
        animations.Udpate(0.1f);
        Assert.Equal("", animations.CurrentAnimation);
        Assert.True(animations.HasFinished);
    }
}