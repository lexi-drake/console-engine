namespace tests;

using ConsoleEngine.Core;
using ConsoleEngine.RenderBlocks;

public class ImageBlockShould
{
    [Fact]
    public void RenderRGBAAsExpected()
    {
        var img = ImageBlock.FromPNG("../../../test_assets/rgba.png");
        img.Update();

        var t = new EngineColor() { R = 0, G = 0, B = 0, A = 0 };
        var g = new EngineColor() { R = 64, G = 148, B = 56, A = 255 };
        var m = new EngineColor() { R = 225, G = 13, B = 142, A = 255 };
        var y = new EngineColor() { R = 219, G = 210, B = 51, A = 255 };
        var b = new EngineColor() { R = 51, G = 80, B = 219, A = 255 };

        var buffer = new TileData[img.AbsWidth * img.AbsHeight];
        img.RenderToBuffer(buffer, img.AbsWidth, img.AbsHeight, 0, 0);
        var expected = new EngineColor[] { y, t, t, m, t, y, m, t, t, g, b, t, g, t, t, b };

        Assert.Equal(6, img.Img.Metadata.ColorType);
        Assert.Equal(expected.Length * 2, buffer.Length);
        for (var i = 0; i < expected.Length; i++)
        {
            var index = i * 2;
            Assert.Equal(expected[i], buffer[index].TileColor.BackgroundColor);
            Assert.Equal(expected[i], buffer[index].TileColor.ForegroundColor);

            Assert.Equal(expected[i], buffer[index + 1].TileColor.BackgroundColor);
            Assert.Equal(expected[i], buffer[index + 1].TileColor.ForegroundColor);
        }
    }

    [Fact]
    public void RenderGAAsExpected()
    {
        var img = ImageBlock.FromPNG("../../../test_assets/ga.png");
        img.Update();

        var t = new EngineColor() { R = 0, G = 0, B = 0, A = 0 };
        var tl = new EngineColor() { R = 200, G = 200, B = 200, A = 255 };
        var tr = new EngineColor() { R = 67, G = 67, B = 67, A = 255 };
        var bl = new EngineColor() { R = 123, G = 123, B = 123, A = 255 };
        var br = new EngineColor() { R = 83, G = 83, B = 83, A = 255 };

        var buffer = new TileData[img.AbsWidth * img.AbsHeight];
        img.RenderToBuffer(buffer, img.AbsWidth, img.AbsHeight, 0, 0);
        var expected = new EngineColor[] { tl, t, t, tr, t, tl, tr, t, t, bl, br, t, bl, t, t, br };

        Assert.Equal(4, img.Img.Metadata.ColorType);
        Assert.Equal(expected.Length * 2, buffer.Length);
        for (var i = 0; i < expected.Length; i++)
        {
            var index = i * 2;
            Assert.Equal(expected[i], buffer[index].TileColor.BackgroundColor);
            Assert.Equal(expected[i], buffer[index].TileColor.ForegroundColor);

            Assert.Equal(expected[i], buffer[index + 1].TileColor.BackgroundColor);
            Assert.Equal(expected[i], buffer[index + 1].TileColor.ForegroundColor);
        }
    }

    [Fact]
    public void RenderIndexedAsExpected()
    {
        var img = ImageBlock.FromPNG("../../../test_assets/indexed.png");
        img.Update();

        var t = new EngineColor() { R = 0, G = 0, B = 0, A = 255 };
        var g = new EngineColor() { R = 64, G = 148, B = 56, A = 255 };
        var m = new EngineColor() { R = 225, G = 13, B = 142, A = 255 };
        var y = new EngineColor() { R = 219, G = 210, B = 51, A = 255 };
        var b = new EngineColor() { R = 51, G = 80, B = 219, A = 255 };

        var buffer = new TileData[img.AbsWidth * img.AbsHeight];
        img.RenderToBuffer(buffer, img.AbsWidth, img.AbsHeight, 0, 0);
        var expected = new EngineColor[] { y, t, t, m, t, y, m, t, t, g, b, t, g, t, t, b };

        Assert.Equal(3, img.Img.Metadata.ColorType);
        Assert.Equal(expected.Length * 2, buffer.Length);
        for (var i = 0; i < expected.Length; i++)
        {
            var index = i * 2;
            Assert.Equal(expected[i], buffer[index].TileColor.BackgroundColor);
            Assert.Equal(expected[i], buffer[index].TileColor.ForegroundColor);

            Assert.Equal(expected[i], buffer[index + 1].TileColor.BackgroundColor);
            Assert.Equal(expected[i], buffer[index + 1].TileColor.ForegroundColor);
        }
    }

    // [Fact]
    // public void RenderGAsExpected()
    // {
    // TODO: figure out how to create PNG with color-type == 0
    // }

    // [Fact]
    // public void RenderRGBAsExpected()
    // {
    // TODO: figure out how to create PNG with color-type == 2
    // }
}