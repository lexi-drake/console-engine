namespace tests;

using ConsoleEngine.Core;
using ConsoleEngine.RenderBlocks;

public class TextBlockShould
{
    private string _s = "test";
    private EngineColor _bColor = new EngineColor() { R = 100, G = 200, B = 10, A = 255 };
    private EngineColor _fColor = new EngineColor() { R = 200, G = 10, B = 100, A = 255 };

    private FillBlock _parent = new FillBlock()
    {
        Width = new Measurement() { Value = 100, Type = Measurement.MeasurementType.Absolute },
        Height = new Measurement() { Value = 100, Type = Measurement.MeasurementType.Absolute },
    };

    public TextBlockShould()
    {
        _parent.Update();
    }

    [Fact]
    public void RenderTextAsExpected()
    {
        var textBlock = CreateTextBlock();
        _parent.PushRenderBlock(textBlock);
        _parent.Update();

        var buffer = new TileData[textBlock.AbsWidth * textBlock.AbsHeight];
        textBlock.RenderToBuffer(buffer, textBlock.AbsWidth, textBlock.AbsHeight, 0, 0);
        for (var i = 0; i < _s.Length; i++)
        {
            var tile = buffer[i];
            Assert.Equal(_s[i], tile.TileCharacter);
            Assert.Equal(_bColor, tile.TileColor.BackgroundColor);
            Assert.Equal(_fColor, tile.TileColor.ForegroundColor);
        }
    }

    [Fact]
    public void RenderTextWithAbsoluteMargins()
    {
        var margins = new Margins()
        {
            Left = new Measurement() { Value = 2, Type = Measurement.MeasurementType.Absolute },
            Top = new Measurement() { Value = 2, Type = Measurement.MeasurementType.Absolute },
            Right = new Measurement() { Value = 2, Type = Measurement.MeasurementType.Absolute },
            Bottom = new Measurement() { Value = 2, Type = Measurement.MeasurementType.Absolute },
        };
        var textBlock = CreateTextBlock(margins);
        _parent.PushRenderBlock(textBlock);
        textBlock.Update();

        var buffer = new TileData[textBlock.AbsWidth * textBlock.AbsHeight];
        textBlock.RenderToBuffer(buffer, textBlock.AbsWidth, textBlock.AbsHeight, 0, 0);
        for (var x = 0; x < _s.Length; x++)
        {
            var index = textBlock.AbsTopMargin * textBlock.AbsWidth + textBlock.AbsLeftMargin + x;
            var tile = buffer[index];
            Assert.Equal(_s[x], tile.TileCharacter);
            Assert.Equal(_bColor, tile.TileColor.BackgroundColor);
            Assert.Equal(_fColor, tile.TileColor.ForegroundColor);
        }
    }

    [Fact]
    public void RenderTextWithPercentMargins()
    {
        var margins = new Margins()
        {
            Left = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Percent },
            Top = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Percent },
            Right = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Percent },
            Bottom = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Percent },
        };
        var textBlock = CreateTextBlock(margins);
        _parent.PushRenderBlock(textBlock);
        textBlock.Update();

        Assert.Equal(1, textBlock.AbsLeftMargin);
        Assert.Equal(1, textBlock.AbsTopMargin);
        Assert.Equal(1, textBlock.AbsRightMargin);
        Assert.Equal(1, textBlock.AbsBottomMargin);

        var buffer = new TileData[textBlock.AbsWidth * textBlock.AbsHeight];
        textBlock.RenderToBuffer(buffer, textBlock.AbsWidth, textBlock.AbsHeight, 0, 0);
        for (var x = 0; x < _s.Length; x++)
        {
            var index = textBlock.AbsTopMargin * textBlock.AbsWidth + textBlock.AbsLeftMargin + x;
            var tile = buffer[index];
            Assert.Equal(_s[x], tile.TileCharacter);
            Assert.Equal(_bColor, tile.TileColor.BackgroundColor);
            Assert.Equal(_fColor, tile.TileColor.ForegroundColor);
        }
    }

    [Fact]
    public void WrapLinesCorrectly()
    {
        var textBlock = CreateTextBlock();
        textBlock.Text = "this is multiple lines";
        _parent.PushRenderBlock(textBlock);
        _parent.Update();

        var buffer = new TileData[textBlock.AbsWidth * textBlock.AbsHeight];
        textBlock.RenderToBuffer(buffer, textBlock.AbsWidth, textBlock.AbsHeight, 0, 0);
        var expectedFirstCharacter = new char[] { 't', 'm', 'l' };

        for (var y = 0; y < 3; y++)
        {
            var firstCharIndex = y * textBlock.AbsWidth;
            Assert.Equal(expectedFirstCharacter[y], buffer[firstCharIndex].TileCharacter);
        }
    }

    [Fact]
    public void WrapLinesWithMargins()
    {
        var textBlock = CreateTextBlock(new Margins()
        {
            Top = new Measurement() { Value = 3, Type = Measurement.MeasurementType.Absolute },
            Left = new Measurement() { Value = 3, Type = Measurement.MeasurementType.Absolute },
            Bottom = new Measurement() { Value = 3, Type = Measurement.MeasurementType.Absolute },
            Right = new Measurement() { Value = 3, Type = Measurement.MeasurementType.Absolute },
        });

        textBlock.Text = "this is multiple lines";
        _parent.PushRenderBlock(textBlock);
        _parent.Update();

        var buffer = new TileData[textBlock.AbsWidth * textBlock.AbsHeight];
        textBlock.RenderToBuffer(buffer, textBlock.AbsWidth, textBlock.AbsHeight, 0, 0);
        var expectedFirstCharacter = new char[] { 't', 'i', 'l' };

        for (var y = 0; y < 3; y++)
        {
            var firstCharIndex = (y + textBlock.AbsTopMargin) * textBlock.AbsWidth + textBlock.AbsLeftMargin;
            Assert.Equal(expectedFirstCharacter[y], buffer[firstCharIndex].TileCharacter);
        }
    }

    private TextBlock CreateTextBlock(Margins? margins = null) => new()
    {
        Text = _s,
        TileColor = new TileColor()
        {
            ForegroundColor = _fColor,
            BackgroundColor = _bColor
        },
        Width = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Absolute },
        Height = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Absolute },
        Margins = margins ?? Margins.Zero
    };
}