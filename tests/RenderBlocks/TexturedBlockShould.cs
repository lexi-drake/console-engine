namespace tests;

using ConsoleEngine.Core;
using ConsoleEngine.RenderBlocks;

public class TexturedBlockShould
{
    [Fact]
    public void DistributeAvailableColorsWithNoChars()
    {
        var block = new TexturedBlock(0.5f, [EngineColor.White, EngineColor.Black])
        {
            Width = new Measurement() { Type = Measurement.MeasurementType.Absolute, Value = 10 },
            Height = new Measurement() { Type = Measurement.MeasurementType.Absolute, Value = 10 }
        };
        block.Update();

        var buffer = new TileData[block.AbsWidth * block.AbsHeight];
        block.RenderToBuffer(buffer, block.AbsWidth, block.AbsHeight, 0, 0);
        foreach (var d in buffer)
        {
            Assert.NotEqual(EngineColor.Transparent, d.TileColor.BackgroundColor);
            Assert.NotEqual(EngineColor.Transparent, d.TileColor.ForegroundColor);
            Assert.True(EngineColor.White == d.TileColor.BackgroundColor || EngineColor.Black == d.TileColor.BackgroundColor);
            Assert.True(EngineColor.White == d.TileColor.ForegroundColor || EngineColor.Black == d.TileColor.ForegroundColor);
            Assert.Null(d.TileCharacter);
        }
    }

    [Fact]
    public void DistributeAvailableColorsWithChars()
    {
        var block = new TexturedBlock(0.5f, [EngineColor.White, EngineColor.Black], ['.', ','])
        {
            Width = new Measurement() { Type = Measurement.MeasurementType.Absolute, Value = 10 },
            Height = new Measurement() { Type = Measurement.MeasurementType.Absolute, Value = 10 }
        };
        block.Update();

        var buffer = new TileData[block.AbsWidth * block.AbsHeight];
        block.RenderToBuffer(buffer, block.AbsWidth, block.AbsHeight, 0, 0);
        foreach (var d in buffer)
        {
            Assert.NotEqual(EngineColor.Transparent, d.TileColor.BackgroundColor);
            Assert.NotEqual(EngineColor.Transparent, d.TileColor.ForegroundColor);
            Assert.True(EngineColor.White == d.TileColor.BackgroundColor || EngineColor.Black == d.TileColor.BackgroundColor);
            Assert.True(EngineColor.White == d.TileColor.ForegroundColor || EngineColor.Black == d.TileColor.ForegroundColor);
            Assert.True('.' == d.TileCharacter || ',' == d.TileCharacter);
        }
    }

    [Fact]
    public void GenerateRandomColors()
    {
        var block = new TexturedBlock(0.5f)
        {
            Width = new Measurement() { Type = Measurement.MeasurementType.Absolute, Value = 10 },
            Height = new Measurement() { Type = Measurement.MeasurementType.Absolute, Value = 10 }
        };
        block.Update();

        var buffer = new TileData[block.AbsWidth * block.AbsHeight];
        block.RenderToBuffer(buffer, block.AbsWidth, block.AbsHeight, 0, 0);
        foreach (var d in buffer)
        {
            Assert.NotEqual(EngineColor.Transparent, d.TileColor.BackgroundColor);
            Assert.NotEqual(EngineColor.Transparent, d.TileColor.ForegroundColor);
            Assert.Null(d.TileCharacter);
        }
    }
}