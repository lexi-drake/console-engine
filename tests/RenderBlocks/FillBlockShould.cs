namespace tests;

using ConsoleEngine.Core;
using ConsoleEngine.RenderBlocks;

public class FillBlockShould
{
    private readonly char _fillCharacter = '#';
    private EngineColor _bColor = new EngineColor() { R = 100, G = 200, B = 10, A = 255 };
    private EngineColor _fColor = new EngineColor() { R = 200, G = 10, B = 100, A = 255 };

    [Fact]
    public void ReturnAsExpected()
    {
        var fillBlock = CreateFillBlock(_fillCharacter);
        fillBlock.Update();

        var buffer = new TileData[fillBlock.AbsWidth * fillBlock.AbsHeight];
        fillBlock.RenderToBuffer(buffer, fillBlock.AbsWidth, fillBlock.AbsHeight, 0, 0);

        foreach (var data in buffer)
        {
            Assert.Equal(_fillCharacter, data.TileCharacter);
            Assert.Equal(_fColor, data.TileColor.ForegroundColor);
            Assert.Equal(_bColor, data.TileColor.BackgroundColor);
        }
    }

    [Fact]
    public void IgnoresOutsideOfBoundsGT()
    {
        var fillBlock = CreateFillBlock('t');
        var child = CreateFillBlock('c');
        child.Left = new Measurement() { Value = 2, Type = Measurement.MeasurementType.Absolute };
        child.Top = new Measurement() { Value = 2, Type = Measurement.MeasurementType.Absolute };
        fillBlock.PushRenderBlock(child);

        fillBlock.Update();

        var buffer = new TileData[fillBlock.AbsWidth * fillBlock.AbsHeight];
        fillBlock.RenderToBuffer(buffer, fillBlock.AbsWidth, fillBlock.AbsHeight, 0, 0);
        Assert.NotEqual(fillBlock.FillCharacter, child.FillCharacter);
        Assert.Equal(fillBlock.FillCharacter, buffer[0].TileCharacter);
        Assert.Equal(child.FillCharacter, buffer[99].TileCharacter);
    }

    [Fact]
    public void IgnoresOutsideOfBoundsLT()
    {
        var fillBlock = CreateFillBlock('t');
        var child = CreateFillBlock('c');
        child.Left = new Measurement() { Value = -2, Type = Measurement.MeasurementType.Absolute };
        child.Top = new Measurement() { Value = -2, Type = Measurement.MeasurementType.Absolute };
        fillBlock.PushRenderBlock(child);

        fillBlock.Update();

        var buffer = new TileData[fillBlock.AbsWidth * fillBlock.AbsHeight];
        fillBlock.RenderToBuffer(buffer, fillBlock.AbsWidth, fillBlock.AbsHeight, 0, 0);
        Assert.NotEqual(fillBlock.FillCharacter, child.FillCharacter);
        Assert.Equal(fillBlock.FillCharacter, buffer[99].TileCharacter);
        Assert.Equal(child.FillCharacter, buffer[0].TileCharacter);
    }

    private FillBlock CreateFillBlock(char fillCharacter) => new()
    {
        FillCharacter = fillCharacter,
        TileColor = new TileColor()
        {
            ForegroundColor = _fColor,
            BackgroundColor = _bColor
        },
        Width = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Absolute },
        Height = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Absolute }
    };
}