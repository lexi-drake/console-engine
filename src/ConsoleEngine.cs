namespace ConsoleEngine;

using ConsoleEngine.Core;
using ConsoleEngine.RenderBlocks;
using System.Text;

/// <summary>
/// Class <c>ConsoleRenderEngine</c> manages the core functionality of rendering
/// and reading input.
/// </summary>
public class ConsoleRenderEngine
{
    private const char Escape = (char)0x1b;
    private readonly RenderBlock _root;
    private readonly TextWriter _hiddenWriter = new HiddenTextWriter();

    public ConsoleRenderEngine(EngineColor backgroundColor, EngineColor foregroundColor)
    {
        _root = new FillBlock()
        {
            Id = "root",
            TileColor = new TileColor
            {
                BackgroundColor = backgroundColor,
                ForegroundColor = foregroundColor,
            },
            Top = new Measurement() { Value = 0, Type = Measurement.MeasurementType.Absolute },
            Left = new Measurement() { Value = 0, Type = Measurement.MeasurementType.Absolute },
            Width = new Measurement() { Value = 100, Type = Measurement.MeasurementType.Percent },
            Height = new Measurement() { Value = 100, Type = Measurement.MeasurementType.Percent },
            FillCharacter = TileData.WhiteSpace
        };
        AppDomain.CurrentDomain.ProcessExit += (object? sender, EventArgs e) =>
        {
            Console.CursorVisible = true;
        };
    }

    /// <summary>
    /// Method <c>PushBlockToRoot</c> adds the parameter <c>block</c> as a child of the root 
    /// <c>RenderBlock</c>
    /// </summary>
    /// <param name="block">the <c>RenderBlock</c> to add as a child of root</param>
    public void PushBlockToRoot(RenderBlock block)
    {
        _root.PushRenderBlock(block);
    }

    /// <summary>
    /// Method <c>Render</c> renders the root <c>RenderBlock</c>
    /// and all child blocks
    /// </summary>
    public void Render()
    {
        Setup();
        var buffer = BuildBuffer(0, 0);
        WriteBufferToConsole(buffer);
    }

    public void Observe(int xOffset, int yOffset)
    {
        Setup();
        var buffer = BuildBuffer(xOffset, yOffset);
        WriteBufferToConsole(buffer);
    }

    public void Setup()
    {
        Console.CursorVisible = false;
        Console.SetCursorPosition(0, 0);
        _root.Update();
    }

    private TileData[] BuildBuffer(int xOffset, int yOffset)
    {
        var buffer = new TileData[_root.AbsWidth * _root.AbsHeight];
        _root.RenderToBuffer(buffer, _root.AbsWidth, _root.AbsHeight, xOffset, yOffset);
        return FitBufferToFrame(buffer, _root.AbsWidth, _root.AbsHeight);
    }

    public static TileData[] FitBufferToFrame(TileData[] buffer, int width, int height)
    {
        for (int y = 0; y < height - 1; y++)
        {
            var index = y * width + width - 1;
            buffer[index].TileCharacter = '\n';
        }
        return buffer;
    }

    public static void WriteBufferToConsole(TileData[] buffer)
    {
        var startIndex = 0;
        var windowBuffer = new StringBuilder();
        while (startIndex < buffer.Length)
        {
            var startData = buffer[startIndex];
            var backgroundColor = startData.TileColor.BackgroundColor;
            var foregroundColor = startData.TileColor.ForegroundColor;
            var index = startIndex;
            var writeBuffer = new StringBuilder();
            while (index < buffer.Length)
            {
                var data = buffer[index];
                if (data.TileColor.BackgroundColor != backgroundColor || data.TileColor.ForegroundColor != foregroundColor)
                {
                    break;
                }
                index++;
                writeBuffer.Append(data.TileCharacterOrDefault);
            }
            var setForeground = $"{Escape}[38;2;{foregroundColor.R};{foregroundColor.G};{foregroundColor.B}m";
            var setBackground = $"{Escape}[48;2;{backgroundColor.R};{backgroundColor.G};{backgroundColor.B}m";
            windowBuffer.Append($"{setForeground}{setBackground}{writeBuffer}");
            startIndex = index;
        }
        Console.Write(windowBuffer);
    }

    /// <summary>
    /// Method <c>GetInput</c> reads the first keypress in a non-blocking way. If a key is 
    /// read, the input-buffer is cleared.
    /// </summary>
    /// <returns>the first key pressed if available; <c>null</c> otherwise</returns>
    public char? GetInput()
    {
        char? readChar = null;
        if (Console.KeyAvailable)
        {
            var cOut = Console.Out;
            // Hide out while reading keypresses.
            Console.SetOut(_hiddenWriter);

            var key = Console.ReadKey();
            readChar = key.KeyChar;

            // Spin out the rest of the buffer so that we only read one character.
            while (Console.KeyAvailable)
            {
                Console.ReadKey();
            }
            Console.SetOut(cOut);
        }
        return readChar;
    }
}

internal class HiddenTextWriter : TextWriter
{
    public override Encoding Encoding => throw new NotImplementedException();
}