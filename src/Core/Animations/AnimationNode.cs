using System.Reflection;

namespace ConsoleEngine.Core.Animations;
public struct AnimationNode
{
    public Action Behavior;
}