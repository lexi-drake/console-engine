namespace ConsoleEngine.Core.Animations;

public class Animation(List<AnimationFrame> animationFrames, bool looping = false)
{
    protected readonly List<AnimationFrame> _animationFrames = animationFrames;
    public int CurrentFrame { get; set; } = 0;
    public int FrameCount => _animationFrames.Count;
    public bool Looping = looping;
    public bool Paused = false;
    public bool HasFinished => CurrentFrame == _animationFrames.Count - 1
        && !Looping
        && _animationTime >= _animationFrames[CurrentFrame].FrameDuration;
    protected double _animationTime = 0;

    internal void ApplyCurrentFrame()
    {
        foreach (var node in _animationFrames[CurrentFrame].Nodes)
        {
            var behavior = node.Behavior;
            if (behavior is null)
                return;

            try
            {
                behavior();
            }
            catch (Exception)
            {
                // Ignore
            }
        }
    }

    private void AdvanceFrame()
    {
        while (_animationTime >= _animationFrames[CurrentFrame].FrameDuration && !Paused)
        {
            if (CurrentFrame < _animationFrames.Count - 1)
            {
                _animationTime -= _animationFrames[CurrentFrame].FrameDuration;
                CurrentFrame++;
            }
            else if (Looping)
            {
                _animationTime -= _animationFrames[CurrentFrame].FrameDuration;
                CurrentFrame = (CurrentFrame + 1) % _animationFrames.Count;
            }
            else
            {
                Paused = true;
            }
            if (!Paused)
                ApplyCurrentFrame();
        }
    }

    public void Reset()
    {
        _animationTime = 0;
        CurrentFrame = 0;
        ApplyCurrentFrame();
    }

    public void Update(double deltaTime)
    {
        if (Paused) return;

        _animationTime += deltaTime;
        if (_animationTime >= _animationFrames[CurrentFrame].FrameDuration)
        {
            AdvanceFrame();
        }
    }
}
