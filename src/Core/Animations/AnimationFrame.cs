namespace ConsoleEngine.Core.Animations;

public struct AnimationFrame
{
    public double FrameDuration;
    public IEnumerable<AnimationNode> Nodes;
}