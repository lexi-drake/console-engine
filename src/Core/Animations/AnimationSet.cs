namespace ConsoleEngine.Core.Animations;

public class AnimationSet
{
    public Dictionary<string, Animation> Animations { get; } = [];
    public float AnimationSpeed { get; set; } = 1;
    public string CurrentAnimation { get; private set; } = "";
    public bool HasFinished => !Animations.ContainsKey(CurrentAnimation) || Animations[CurrentAnimation].HasFinished;
    public bool IsInLastFrame => !Animations.ContainsKey(CurrentAnimation) || Animations[CurrentAnimation].CurrentFrame == Animations[CurrentAnimation].FrameCount - 1;
    public void Play(string animation, bool fromStart = true)
    {
        if (!Animations.ContainsKey(animation)) return;

        if (Animations.ContainsKey(CurrentAnimation))
        {
            Animations[CurrentAnimation].Paused = true;
        }

        CurrentAnimation = animation;
        if (fromStart)
        {
            Animations[CurrentAnimation].Reset();
        }
        else
        {
            Animations[CurrentAnimation].ApplyCurrentFrame();
        }
        Animations[CurrentAnimation].Paused = false;
    }

    public void Pause()
    {
        if (!Animations.ContainsKey(CurrentAnimation)) return;
        Animations[CurrentAnimation].Paused = true;
    }

    public void Udpate(float deltaTime)
    {
        if (!Animations.ContainsKey(CurrentAnimation)) return;

        Animations[CurrentAnimation].Update(deltaTime * AnimationSpeed);
    }
}