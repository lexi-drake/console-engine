namespace ConsoleEngine.Core;

/// <summary>
/// Class <c>Margins</c> represents the distance from the edge of <c>TextBlock</c> 
/// blocks that text should be rendered.
/// </summary>
public struct Margins
{
    public Measurement Left;
    public Measurement Right;
    public Measurement Top;
    public Measurement Bottom;
    public readonly static Margins Zero = new()
    {
        Left = Measurement.Zero,
        Right = Measurement.Zero,
        Top = Measurement.Zero,
        Bottom = Measurement.Zero
    };
}
