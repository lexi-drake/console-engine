namespace ConsoleEngine.Core.Png;

public readonly struct PNG(IHDR metadata, EngineColor[] imgData)
{
    public readonly IHDR Metadata = metadata;
    public readonly EngineColor[] ImageData = imgData;

    public static PNG From(string fileName)
    {
        var file = File.ReadAllBytes(fileName) ?? throw new ArgumentException($"Cannot find file {fileName}");
        var decoder = new PNGDecoder(file);
        return decoder.Decode();
    }
}

