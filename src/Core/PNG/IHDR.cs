namespace ConsoleEngine.Core.Png;

public struct IHDR
{
    public int Width;
    public int Height;
    public int BitDepth;
    public int ColorType;
    public int CompressionMethod;
    public int FilterMethod;
    public int InterlaceMethod;
}