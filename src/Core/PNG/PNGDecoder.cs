using System.IO.Compression;

namespace ConsoleEngine.Core.Png;

internal class PNGDecoder(byte[] file)
{
    private const int HeaderLength = 8;
    private const int LengthBytes = 4;
    private const int TypeBytes = 4;
    private const int CRCBytes = 4;
    private const string IDATType = "49444154";
    private const string PLTEType = "504C5445";
    private readonly string _hex = Convert.ToHexString(file);
    private int _decoderIndex = HeaderLength;
    private IHDR _ihdr;
    private int BitDepthBackBytes => _ihdr.BitDepth < 8 ? 1 : 4;

    private PNGChunk ParseChunk(int startIndex)
    {
        var length = int.Parse(_hex.Substring(startIndex * 2, LengthBytes * 2), System.Globalization.NumberStyles.HexNumber);
        var type = _hex.Substring((startIndex + LengthBytes) * 2, TypeBytes * 2);
        var dataStartIndex = startIndex + LengthBytes + TypeBytes;
        var data = _hex.Substring(dataStartIndex * 2, length * 2);

        _decoderIndex += length + LengthBytes + TypeBytes + CRCBytes;
        return new PNGChunk()
        {
            Length = length,
            Type = type,
            Data = data
        };
    }

    private IHDR ParseIHDR()
    {
        var ihdr = ParseChunk(_decoderIndex);

        var width = int.Parse(ihdr.Data.Substring(0, 8), System.Globalization.NumberStyles.HexNumber);
        var height = int.Parse(ihdr.Data.Substring(8, 8), System.Globalization.NumberStyles.HexNumber);
        var bitDepth = int.Parse(ihdr.Data.Substring(16, 2), System.Globalization.NumberStyles.HexNumber);
        var colorType = int.Parse(ihdr.Data.Substring(18, 2), System.Globalization.NumberStyles.HexNumber);
        var compressionMethod = int.Parse(ihdr.Data.Substring(20, 2), System.Globalization.NumberStyles.HexNumber);
        var filterMethod = int.Parse(ihdr.Data.Substring(22, 2), System.Globalization.NumberStyles.HexNumber);
        var interlaceMethod = int.Parse(ihdr.Data.Substring(24, 2), System.Globalization.NumberStyles.HexNumber);

        return new IHDR()
        {
            Width = width,
            Height = height,
            BitDepth = bitDepth,
            ColorType = colorType,
            CompressionMethod = compressionMethod,
            FilterMethod = filterMethod,
            InterlaceMethod = interlaceMethod
        };
    }

    internal static void NoFilterScanLine(int[] scanLineBytes, int currentScanLine, int[] unfiltered)
    {
        for (var i = 0; i < scanLineBytes.Length; i++)
        {
            var unfilteredIndex = currentScanLine * scanLineBytes.Length + i;
            unfiltered[unfilteredIndex] = scanLineBytes[i];
        }
    }

    internal static void SubFilterScanLine(int[] scanLineBytes, int currentScanLine, int[] unfiltered, int bitDepthBackBytes)
    {
        for (var i = 0; i < scanLineBytes.Length; i++)
        {
            var fX = scanLineBytes[i];
            var aIndex = currentScanLine * scanLineBytes.Length + i - bitDepthBackBytes;
            var oA = i < bitDepthBackBytes ? 0 : unfiltered[aIndex];

            var unfilteredIndex = currentScanLine * scanLineBytes.Length + i;
            var oX = fX + oA;
            unfiltered[unfilteredIndex] = oX % 256;
        }
    }

    internal static void UpFilterScanLine(int[] scanLineBytes, int currentScanLine, int[] unfiltered)
    {
        var previousScanLine = currentScanLine - 1;
        for (var i = 0; i < scanLineBytes.Length; i++)
        {
            var fX = scanLineBytes[i];
            var bIndex = previousScanLine * scanLineBytes.Length + i;
            var oB = bIndex < 0 ? 0 : unfiltered[bIndex];

            var unfilteredIndex = currentScanLine * scanLineBytes.Length + i;

            var oX = fX + oB;
            unfiltered[unfilteredIndex] = oX % 256;
        }
    }

    internal static void AverageFilterScanLine(int[] scanLineBytes, int currentScanLine, int[] unfiltered, int bitDepthBackBytes)
    {
        var previousScanLine = currentScanLine - 1;
        for (var i = 0; i < scanLineBytes.Length; i++)
        {
            var fX = scanLineBytes[i];
            var aIndex = currentScanLine * scanLineBytes.Length + i - bitDepthBackBytes;
            var oA = i < bitDepthBackBytes ? 0 : unfiltered[aIndex];
            var bIndex = previousScanLine * scanLineBytes.Length + i;
            var oB = bIndex < 0 ? 0 : unfiltered[bIndex];
            var unfilteredIndex = currentScanLine * scanLineBytes.Length + i;
            var oX = fX + (oA + oB) / 2;
            unfiltered[unfilteredIndex] = oX % 256;
        }
    }

    internal static void PaethFilterScanLine(int[] scanLineBytes, int currentScanLine, int[] unfiltered, int bitDepthBackBytes)
    {
        var previousScanLine = currentScanLine - 1;
        for (var i = 0; i < scanLineBytes.Length; i++)
        {
            var fX = scanLineBytes[i];
            var aIndex = currentScanLine * scanLineBytes.Length + i - bitDepthBackBytes;
            var oA = i < bitDepthBackBytes ? 0 : unfiltered[aIndex];
            var bIndex = previousScanLine * scanLineBytes.Length + i;
            var oB = bIndex < 0 ? 0 : unfiltered[bIndex];
            var cIndex = bIndex - bitDepthBackBytes;
            var oC = i < bitDepthBackBytes ? 0 : unfiltered[cIndex];

            var p = oA + oB - oC;
            var pA = Math.Abs(p - oA);
            var pB = Math.Abs(p - oB);
            var pC = Math.Abs(p - oC);

            var aCond = pA <= pB && pA <= pC;
            var bCond = pB <= pC;
            var pR = aCond ? oA : bCond ? oB : oC;

            var unfilteredIndex = currentScanLine * scanLineBytes.Length + i;
            var oX = fX + pR;
            unfiltered[unfilteredIndex] = oX % 256;
        }
    }

    private int GetBytesPerPixel() => _ihdr.ColorType switch
    {
        0 => _ihdr.BitDepth,        // G
        2 => _ihdr.BitDepth * 3,    // RGB
        3 => _ihdr.BitDepth,
        4 => _ihdr.BitDepth * 2,    // GA
        6 => _ihdr.BitDepth * 4,    // RGBA
        _ => throw new ArgumentException($"invalid color type {_ihdr.ColorType}; see https://www.w3.org/TR/2003/REC-PNG-20031110/#11IHDR for more information.")
    } / 8;

    private int[] Unfilter(int[] filtered)
    {
        if (_ihdr.FilterMethod != 0)
        {
            throw new NotImplementedException($"filter method {_ihdr.FilterMethod} is not supported");
        }

        var bytesPerPixel = GetBytesPerPixel();
        if (bytesPerPixel <= 0)
        {
            throw new NotImplementedException($"bit depth of {_ihdr.BitDepth} is not supported; bit depth of at least {sizeof(byte)} required");
        }
        var iPerScanLine = _ihdr.Width * bytesPerPixel;

        var unfiltered = new int[bytesPerPixel * _ihdr.Width * _ihdr.Height];
        var currentScanLine = 0;
        var index = 0;
        while (currentScanLine < _ihdr.Height)
        {
            var filterFunction = filtered[index];
            index++;
            var scanLine = filtered[index..(index + iPerScanLine)];
            switch (filterFunction)
            {
                case 0:
                    NoFilterScanLine(scanLine, currentScanLine, unfiltered);
                    break;
                case 1:
                    SubFilterScanLine(scanLine, currentScanLine, unfiltered, BitDepthBackBytes);
                    break;
                case 2:
                    UpFilterScanLine(scanLine, currentScanLine, unfiltered);
                    break;
                case 3:
                    AverageFilterScanLine(scanLine, currentScanLine, unfiltered, BitDepthBackBytes);
                    break;
                case 4:
                    PaethFilterScanLine(scanLine, currentScanLine, unfiltered, BitDepthBackBytes);
                    break;
                default:
                    throw new ArgumentException($"invalid filter function {filterFunction}; see https://www.w3.org/TR/2003/REC-PNG-20031110/#9Filters for more information");
            }
            index += iPerScanLine;
            currentScanLine++;
        }
        return unfiltered;
    }

    private static void TransformGreyscale(int[] imageBytes, EngineColor[] pixels)
    {
        for (int i = 0; i < pixels.Length; i++)
        {
            var gIndex = i;

            pixels[i] = new EngineColor()
            {
                R = imageBytes[gIndex],
                G = imageBytes[gIndex],
                B = imageBytes[gIndex],
                A = 255
            };
        }
    }

    private static void TransformGreyscaleWithAlpha(int[] imageBytes, EngineColor[] pixels)
    {
        for (int i = 0; i < pixels.Length; i++)
        {
            var gIndex = 2 * i;
            var aIndex = gIndex + 1;

            pixels[i] = new EngineColor()
            {
                R = imageBytes[gIndex],
                G = imageBytes[gIndex],
                B = imageBytes[gIndex],
                A = imageBytes[aIndex]
            };
        }
    }

    private static void TransformTruecolor(int[] imageBytes, EngineColor[] pixels)
    {
        for (int i = 0; i < pixels.Length; i++)
        {

            var rIndex = 3 * i;
            var gIndex = rIndex + 1;
            var bIndex = gIndex + 1;

            pixels[i] = new EngineColor()
            {
                R = imageBytes[rIndex],
                G = imageBytes[gIndex],
                B = imageBytes[bIndex],
                A = 255
            };
        }
    }

    private static void TransformTruecolorWithAlpha(int[] imageBytes, EngineColor[] pixels)
    {
        for (int i = 0; i < pixels.Length; i++)
        {
            var rIndex = 4 * i;
            var gIndex = rIndex + 1;
            var bIndex = gIndex + 1;
            var aIndex = bIndex + 1;

            pixels[i] = new EngineColor()
            {
                R = imageBytes[rIndex],
                G = imageBytes[gIndex],
                B = imageBytes[bIndex],
                A = imageBytes[aIndex]
            };
        }
    }

    private static void TransformIndexed(int[] imageBytes, EngineColor[] pixels, PNGChunk? plte)
    {
        if (!plte.HasValue)
            throw new ArgumentException("indexed color type requires PLTE chunk");

        var bytes = plte.Value.ReadDataAsBytes();
        if (bytes.Length % 3 != 0)
            throw new ArgumentException("length of PLTE chunk must be divisible by 3");

        var colorCount = bytes.Length / 3;
        var colors = new EngineColor[colorCount];
        // According to the spec (https://www.w3.org/TR/2003/REC-PNG-20031110/#11PLTE),
        // the alpha-channel is not included in the plte, so all alpha values are
        // assigned 255
        TransformTruecolor([.. bytes], colors);

        for (var i = 0; i < pixels.Length; i++)
        {
            pixels[i] = colors[imageBytes[i]];
        }
    }

    private void Transform(int[] imageBytes, EngineColor[] pixels, PNGChunk? plte)
    {
        switch (_ihdr.ColorType)
        {
            case 0: TransformGreyscale(imageBytes, pixels); break;
            case 2: TransformTruecolor(imageBytes, pixels); break;
            case 3: TransformIndexed(imageBytes, pixels, plte); break;
            case 4: TransformGreyscaleWithAlpha(imageBytes, pixels); break;
            case 6: TransformTruecolorWithAlpha(imageBytes, pixels); break;
            default:
                throw new ArgumentException($"invalid color type {_ihdr.ColorType}; see https://www.w3.org/TR/2003/REC-PNG-20031110/#11IHDR for more information.");
        }
    }


    private EngineColor[] UnpackImage(byte[] bytes, PNGChunk? plte)
    {
        // Decompress
        var stream = new MemoryStream(bytes);
        var zLibStream = new ZLibStream(stream, CompressionMode.Decompress);
        List<int> filtered = [];
        while (true)
        {
            var next = zLibStream.ReadByte();
            if (next == -1) break;
            filtered.Add(next);
        }
        // De-filter
        var unfiltered = Unfilter([.. filtered]);
        // Convert to colors
        var pixels = new EngineColor[_ihdr.Width * _ihdr.Height];
        Transform(unfiltered, pixels, plte);

        return pixels;
    }

    public PNG Decode()
    {
        _ihdr = ParseIHDR();

        List<PNGChunk> dataChunks = [];
        PNGChunk? plte = null;
        while (_decoderIndex < _hex.Length / 2)
        {
            var nextChunk = ParseChunk(_decoderIndex);
            if (nextChunk.Type == PLTEType)
            {
                plte = nextChunk;
                continue;
            }
            if (nextChunk.Type == IDATType)
            {
                dataChunks.Add(nextChunk);
            }
        }
        var data = from chunk in dataChunks
                   from b in chunk.ReadDataAsBytes()
                   select b;
        var decoded = UnpackImage(data.ToArray(), plte);
        return new PNG(_ihdr, decoded);
    }
}