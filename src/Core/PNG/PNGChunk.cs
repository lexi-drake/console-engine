namespace ConsoleEngine.Core.Png;

internal struct PNGChunk
{
    public int Length;
    public string Type;
    public string Data;
    private byte[]? _bytes;

    public byte[] ReadDataAsBytes()
    {
        if (_bytes is null)
        {
            _bytes = new byte[Data.Length / 2];

            // Hex to decimal
            for (int i = 0; i < _bytes.Length; i++)
            {
                var sub = Data.Substring(i * 2, 2);
                _bytes[i] = byte.Parse(sub, System.Globalization.NumberStyles.HexNumber);
            }
        }
        return _bytes;
    }
}