using System.Diagnostics.CodeAnalysis;

namespace ConsoleEngine.Core;

public struct EngineColor
{
    public int R;
    public int G;
    public int B;
    public int A;

    public override readonly bool Equals([NotNullWhen(true)] object? obj)
    {
        if (obj is null || obj is not EngineColor color) return false;

        return color.R == R && color.G == G && color.B == B && color.A == A;
    }
    public static bool operator ==(EngineColor a, object? obj) => a.Equals(obj);
    public static bool operator !=(EngineColor a, object? obj) => !a.Equals(obj);

    public override readonly int GetHashCode() => base.GetHashCode();

    public static EngineColor operator +(EngineColor a, EngineColor b)
    {
        if (b.A == 255)
            return b;

        if (b.A == 0)
            return a;

        var bRatio = b.A / 255f;
        var aRatio = 1 - bRatio;

        var red = ((int)(bRatio * b.R) + (int)(aRatio * a.R)) % 256;
        var green = ((int)(bRatio * b.G) + (int)(aRatio * a.G)) % 256;
        var blue = ((int)(bRatio * b.B) + (int)(aRatio * a.B)) % 256;

        return new EngineColor()
        {
            R = red,
            G = green,
            B = blue,
            A = 255
        }; ;
    }

    public override readonly string ToString() => $"R:{R},G:{G},B:{B},A:{A}";
    public readonly static EngineColor White = new() { R = 255, G = 255, B = 255, A = 255 };
    public readonly static EngineColor Black = new() { R = 0, G = 0, B = 0, A = 255 };
    public readonly static EngineColor Transparent = new() { R = 255, G = 255, B = 255, A = 0 };
}