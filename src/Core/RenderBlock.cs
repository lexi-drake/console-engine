namespace ConsoleEngine.Core;

public abstract class RenderBlock
{
    public string Id { get; set; } = Guid.NewGuid().ToString();
    public RenderBlock? Parent { get; set; }
    public TileColor TileColor { get; set; } = new TileColor();
    public int AbsLeft { get; private set; } = 0;
    public Measurement Left { get; set; } = Measurement.Zero;
    public int AbsTop { get; private set; } = 0;
    public Measurement Top { get; set; } = Measurement.Zero;
    public int AbsWidth { get; private set; } = 0;
    public Measurement Width { get; set; } = Measurement.Zero;
    public int AbsHeight { get; private set; } = 0;
    public Measurement Height { get; set; } = Measurement.Zero;
    private readonly Dictionary<string, RenderBlock> _children = [];
    private int _previousParentWidth = -1;
    private int _previousParentHeight = -1;
    protected bool HasResized = false;

    public virtual void Update()
    {
        var parentWidth = Parent is null ? Console.WindowWidth : Parent.AbsWidth;
        var parentHeight = Parent is null ? Console.WindowHeight : Parent.AbsHeight;

        HasResized = true;
        if (_previousParentHeight == parentHeight && _previousParentWidth == parentWidth)
        {
            HasResized = false;
        }

        _previousParentWidth = parentWidth;
        _previousParentHeight = parentHeight;

        AbsLeft = Left.GetAbsoluteValue(parentWidth);
        AbsWidth = Width.GetAbsoluteValue(parentWidth);

        AbsTop = Top.GetAbsoluteValue(parentHeight);
        AbsHeight = Height.GetAbsoluteValue(parentHeight);

        foreach (var child in _children.Values)
        {
            child.Update();
        }
    }

    public void RenderToBuffer(TileData[] buffer, int width, int height, int xOffset, int yOffset)
    {
        xOffset += AbsLeft;
        yOffset += AbsTop;
        var minX = Math.Max(-xOffset, 0);
        var minY = Math.Max(-yOffset, 0);
        for (var y = minY; y < AbsHeight && (y + yOffset) < height; y++)
        {
            for (var x = minX; x < AbsWidth && (x + xOffset) < width; x++)
            {
                var tile = GetLocalTileData(x, y);
                var bufferIndex = (y + yOffset) * width + xOffset + x;
                var bufferTile = buffer[bufferIndex];

                buffer[bufferIndex].TileCharacter = tile.TileCharacter ?? bufferTile.TileCharacter;
                buffer[bufferIndex].TileColor.BackgroundColor += tile.TileColor.BackgroundColor;
                buffer[bufferIndex].TileColor.ForegroundColor += tile.TileColor.ForegroundColor;
            }
        }

        foreach (var child in _children.Values)
        {
            child.RenderToBuffer(buffer, width, height, xOffset, yOffset);
        }
    }

    public bool PushRenderBlock(RenderBlock block)
    {
        block.Parent = this;
        return _children.TryAdd(block.Id, block);
    }

    public void RemoveRenderBlock(string id) => _children.Remove(id);

    protected abstract TileData GetLocalTileData(int x, int y);
}
