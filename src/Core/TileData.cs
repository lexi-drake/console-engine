namespace ConsoleEngine.Core;

public struct TileData
{
    public TileColor TileColor;
    public char? TileCharacter;
    public const char WhiteSpace = ' ';
    public readonly char TileCharacterOrDefault => char.IsWhiteSpace(TileCharacter ?? WhiteSpace) ? WhiteSpace : TileCharacter ?? WhiteSpace;
}