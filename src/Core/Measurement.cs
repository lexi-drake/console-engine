namespace ConsoleEngine.Core;

public struct Measurement
{
    public int Value { get; set; }
    public MeasurementType Type { get; set; }

    public readonly int GetAbsoluteValue(int parentValue)
    {
        if (Type == MeasurementType.Absolute)
        {
            return Value;
        }

        var value = (int)Math.Round(Value / 100f * parentValue);
        return value;
    }

    public enum MeasurementType
    {
        Absolute = 0,
        Percent = 1
    }
    public readonly static Measurement Zero = new() { Value = 0, Type = MeasurementType.Absolute };
}

