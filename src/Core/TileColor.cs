namespace ConsoleEngine.Core;

public struct TileColor
{
    public EngineColor BackgroundColor;
    public EngineColor ForegroundColor;

    public readonly static EngineColor DefaultBackgroundColor = new() { R = 0, G = 0, B = 0, A = 255 };
    public readonly static EngineColor DefaultForegroundColor = new() { R = 255, G = 255, B = 255, A = 255 };
    public readonly override string ToString() => $"back: {BackgroundColor}; fore: {ForegroundColor}";
}
