using ConsoleEngine.Core;

namespace ConsoleEngine.RenderBlocks;

public class TexturedBlock : RenderBlock
{
    private readonly float _chaos;
    private readonly List<EngineColor> _availableColors;
    private readonly List<char?> _availableChars;
    private readonly bool _progressive;
    private int _cacheHeight = 0;
    private int _cacheWidth = 0;
    private int[] _currentForegroundColors = [];
    private int[] _currentBackgroundColors = [];
    private int[] _currentChars = [];

    public TexturedBlock(float chaos, List<EngineColor>? availableColors = null, List<char?>? availableChars = null, bool progressive = true)
    {
        _chaos = chaos;
        if (availableColors is null || !availableColors.Any())
        {
            availableColors = RandomColors();
        }

        availableChars ??= [null];
        _availableColors = availableColors;
        _availableChars = availableChars;
        _progressive = progressive;
    }

    private static List<EngineColor> RandomColors()
    {
        List<EngineColor> availableColors = [];
        var random = new Random();
        var numColors = random.Next(4, 16);
        for (var i = 0; i < numColors; i++)
        {
            availableColors.Add(new EngineColor()
            {
                R = random.Next(256),
                G = random.Next(256),
                B = random.Next(256),
                A = random.Next(256)
            });
        }
        return availableColors;
    }

    private int GetNextIndex(int current, int max)
    {
        var random = new Random();
        if (random.NextSingle() < _chaos)
        {
            if (_progressive)
            {
                current = random.Next() % 2 == 0 ? (current + 1) % max : current - 1;
                if (current < 0) current += max;
            }
            else
            {
                current = random.Next(max);
            }
        }
        return current;
    }

    private void BuildTexture(List<Coordinate> frontier)
    {
        var random = new Random();
        if (!frontier.Any())
        {
            var coord = new Coordinate()
            {
                X = 0,
                Y = 0,
                Char = random.Next(_availableChars.Count),
                ForegroundColor = random.Next(_availableColors.Count),
                BackgroundColor = random.Next(_availableChars.Count)
            };
            frontier.Add(coord);
        }

        while (frontier.Any())
        {
            var next = random.Next(frontier.Count);
            var current = frontier[next];
            frontier.RemoveAt(next);
            var index = GetIndex(current.X, current.Y);
            if (_currentForegroundColors[index] != -1) continue;

            var currentForegroundColor = GetNextIndex(current.ForegroundColor, _availableColors.Count);
            var currentBackgroundColor = GetNextIndex(current.BackgroundColor, _availableColors.Count);
            var currentChar = GetNextIndex(current.Char, _availableChars.Count);

            _currentForegroundColors[index] = currentForegroundColor;
            _currentBackgroundColors[index] = currentBackgroundColor;
            _currentChars[index] = currentChar;

            for (var x = -1; x <= 1; x++)
            {
                var newX = current.X + x;
                if (newX < 0 || newX >= AbsWidth) continue;
                for (var y = -1; y <= 1; y++)
                {
                    var newY = current.Y + y;
                    if (newY < 0 || newY >= AbsHeight) continue;
                    var newIndex = GetIndex(newX, newY);
                    if (_currentForegroundColors[newIndex] == -1)
                    {
                        frontier.Add(new Coordinate()
                        {
                            X = newX,
                            Y = newY,
                            ForegroundColor = currentForegroundColor,
                            BackgroundColor = currentBackgroundColor,
                            Char = currentChar
                        });
                    }
                }
            }
        }
    }

    private int GetIndex(int x, int y) => y * AbsWidth + x;

    private void CheckCache()
    {
        if (AbsHeight == _cacheHeight && AbsWidth == _cacheWidth) return;
        if (AbsWidth == 0 || AbsHeight == 0) return;

        List<Coordinate> frontier = [];
        var product = AbsHeight * AbsWidth;
        var newForegroundColors = new int[product];
        var newBackgroundColors = new int[product];
        var newChars = new int[product];
        for (var y = 0; y < _cacheHeight; y++)
        {
            for (var x = 0; x < _cacheWidth; x++)
            {
                var oldIndex = x * _cacheWidth + x;
                var newIndex = GetIndex(x, y);
                newForegroundColors[newIndex] = _currentForegroundColors[oldIndex];
                newBackgroundColors[newIndex] = _currentBackgroundColors[oldIndex];
                newChars[newIndex] = _currentChars[oldIndex];
                // only do this the last time through
                if (y == _cacheHeight - 1) continue;
                var fxIndex = _cacheHeight + x;
                frontier.Add(new Coordinate()
                {
                    X = x,
                    Y = _cacheHeight,
                    ForegroundColor = newForegroundColors[fxIndex],
                    BackgroundColor = newBackgroundColors[fxIndex],
                    Char = newChars[fxIndex]
                });
            }
            var fyIndex = y * AbsWidth + _cacheWidth - 1;
            frontier.Add(new Coordinate()
            {
                X = _cacheWidth,
                Y = y,
                ForegroundColor = newForegroundColors[fyIndex],
                BackgroundColor = newBackgroundColors[fyIndex],
                Char = newChars[fyIndex]
            });
        }
        for (var y = _cacheHeight; y < AbsHeight; y++)
        {
            for (var x = _cacheWidth; x < AbsWidth; x++)
            {
                var index = GetIndex(x, y);
                newForegroundColors[index] = -1;
                newBackgroundColors[index] = -1;
                newChars[index] = -1;
            }
        }

        _currentForegroundColors = newForegroundColors;
        _currentBackgroundColors = newBackgroundColors;
        _currentChars = newChars;
        BuildTexture(frontier);

        _cacheHeight = AbsHeight;
        _cacheWidth = AbsWidth;
    }


    protected override TileData GetLocalTileData(int x, int y)
    {
        CheckCache();

        var init = _cacheHeight + _cacheWidth > 0;
        var index = GetIndex(x, y);
        return new TileData()
        {
            TileCharacter = init ? _availableChars[_currentChars[index]] : null,
            TileColor = new TileColor()
            {

                ForegroundColor = init ? _availableColors[_currentForegroundColors[index]] : EngineColor.Transparent,
                BackgroundColor = init ? _availableColors[_currentBackgroundColors[index]] : EngineColor.Transparent
            }
        };
    }

    private struct Coordinate
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int ForegroundColor { get; set; }
        public int BackgroundColor { get; set; }
        public int Char { get; set; }
        public static Coordinate Zero = new Coordinate { X = 0, Y = 0, ForegroundColor = 0, BackgroundColor = 0, Char = 0 };
    }
}