using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("tests")]

namespace ConsoleEngine.RenderBlocks;

using ConsoleEngine.Core;

public class TextBlock : RenderBlock
{
    private string _text = "";
    private bool _forceRebuildLines = false;
    public string Text
    {
        get => _text; set
        {
            _text = value;
            _forceRebuildLines = true;
        }
    }
    public Margins Margins;
    internal int AbsLeftMargin { get; private set; }
    internal int AbsRightMargin { get; private set; }
    internal int AbsTopMargin { get; private set; }
    internal int AbsBottomMargin { get; private set; }
    private List<string> _lines = new();

    /// <summary>
    /// Method <c>Update</c> wraps <c>RenderBlock.Update</c> and also updates 
    /// <c>Margin</c> values accordingly
    /// </summary>
    public override void Update()
    {
        base.Update();
        if (!HasResized && !_forceRebuildLines) return;

        AbsLeftMargin = Margins.Left.GetAbsoluteValue(AbsWidth);
        AbsRightMargin = Margins.Right.GetAbsoluteValue(AbsWidth);
        AbsTopMargin = Margins.Top.GetAbsoluteValue(AbsHeight);
        AbsBottomMargin = Margins.Bottom.GetAbsoluteValue(AbsHeight);
        if (_forceRebuildLines)
        {
            BuildLines();
            _forceRebuildLines = false;
        }

    }

    private int ScanWidth => AbsWidth - AbsLeftMargin - AbsRightMargin;
    private int MaxLines => AbsHeight - AbsBottomMargin - AbsTopMargin;

    public int LineCount { get; private set; }

    private static List<List<string>> PredictLines(string text, int scanWidth, int maxLines)
    {
        var words = text.Split(' ');
        var lines = new List<List<string>>();
        var wordIndex = 0;
        var wordsInLine = 0;
        var lineIndex = 0;
        var lineLength = 0;
        while (wordIndex < words.Length && lineIndex < maxLines)
        {
            if (lines.Count <= lineIndex)
            {
                lines.Add([]);
            }

            var word = words[wordIndex];
            if (word.Length > scanWidth)
            {
                lines[lineIndex].Add(word);
                wordIndex++;
                // This will necessarily cause the next branch to evaluate as true, so it's fine to let this fall through.
            }
            if (word.Length + lineLength + wordsInLine > scanWidth)
            {
                lineIndex++;
                lineLength = 0;
                wordsInLine = 0;
                continue;
            }

            lines[lineIndex].Add(word);
            wordsInLine++;
            lineLength += word.Length;
            wordIndex++;
        }
        return lines;
    }

    private void BuildLines()
    {
        var lines = PredictLines(Text, ScanWidth, MaxLines);

        _lines = [];
        foreach (var line in lines)
        {
            _lines.Add(string.Join(' ', line));
        }
        LineCount = _lines.Count;
    }

    public static int PredictLineCount(string text, int scanWidth, int maxLines) =>
        PredictLines(text, scanWidth, maxLines).Count;

    private char GetTileCharacter(int x, int y)
    {
        var localX = x - AbsLeftMargin;
        var localY = y - AbsTopMargin;
        var scanWidth = ScanWidth;
        if (scanWidth > Text.Length)
        {
            var stringIndex = localY * ScanWidth + localX;
            return stringIndex < Text.Length ? Text[stringIndex] : TileData.WhiteSpace;
        }

        if (localY >= _lines.Count) return TileData.WhiteSpace;

        var line = string.Join(' ', _lines[localY]);

        if (localX >= line.Length) return TileData.WhiteSpace;

        return line[localX];
    }

    protected override TileData GetLocalTileData(int x, int y)
    {
        if (AbsLeftMargin > x || AbsTopMargin > y ||
        x >= AbsWidth - AbsRightMargin || y >= AbsHeight - AbsBottomMargin)
        {
            return new TileData()
            {
                TileColor = TileColor,
                TileCharacter = TileData.WhiteSpace
            };
        }
        return new TileData()
        {
            TileColor = TileColor,
            TileCharacter = GetTileCharacter(x, y)
        };
    }
}
