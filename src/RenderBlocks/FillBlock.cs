namespace ConsoleEngine.RenderBlocks;
using ConsoleEngine.Core;

public class FillBlock : RenderBlock
{
    private TileData _tileData = new();

    public char? FillCharacter
    {
        get => _tileData.TileCharacter;
        set => _tileData.TileCharacter = value;

    }
    public new TileColor TileColor
    {
        get => _tileData.TileColor;
        set => _tileData.TileColor = value;
    }
    protected override TileData GetLocalTileData(int x, int y) => _tileData;
}
