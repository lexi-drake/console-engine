using ConsoleEngine.Core;
using ConsoleEngine.Core.Png;

namespace ConsoleEngine.RenderBlocks;

public class ImageBlock : RenderBlock
{

    public PNG Img { get; private set; }

    private ImageBlock(PNG img)
    {
        Img = img;
    }

    public static ImageBlock FromPNG(string fileName)
    {
        var png = PNG.From(fileName);
        return new ImageBlock(png)
        {
            Width = new Measurement() { Value = png.Metadata.Width * 2, Type = Measurement.MeasurementType.Absolute },
            Height = new Measurement() { Value = png.Metadata.Height, Type = Measurement.MeasurementType.Absolute }
        };
    }

    protected override TileData GetLocalTileData(int x, int y)
    {
        var index = y * (AbsWidth / 2) + (x / 2);
        var color = Img.ImageData[index];

        return new TileData()
        {
            TileColor = new TileColor()
            {
                ForegroundColor = color,
                BackgroundColor = color
            },
            TileCharacter = TileData.WhiteSpace
        };
    }
}