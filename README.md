# Console Engine

## What

A small library to simplify rendering-to and interacting-with the terminal.

## Usage

### Basic Usage

```csharp
using ConsoleEngine;
using ConsoleEngine.Core;
using ConsoleEngine.RenderBlocks;

var renderer = new ConsoleRenderEngine(TileColor.DefaultBackgroundColor, TileColor.DefaultForegroundColor);

var fillBlock = new FillBlock() 
{
    // ...
}

renderer.PushBlockToRoot(fillBlock);
renderer.Render();
```

### Rendering multiple blocks

`RenderBlock`s are n-tree-nodes. Child `RenderBlock`s are positioned relative to the parent.

```csharp
// by default, root is the entire terminal window and is "responsive" to window resizing
var renderer = new ConsoleRenderEngine();

var block1 = new FillBlock()
{
    // ...
    Top = new Measurement() { Value = 0; Type = Measure.MeasurementType.Absolute },
    Left = new Measurement() { Value = 2; Type = Measure.MeasurementType.Absolute }
};

var block2 = new TextBlock()
{
    // ..
    Top = new Measurement() { Value = 2; Type = Measure.MeasurementType.Absolute },
    Left = new Measurement() { Value = 2; Type = Measure.MeasurementType.Absolute }
};

renderer.PushBlockToRoot(block1);
renderer.PushBlockToRoot(block2);

// global value of block1 top-left = {2, 0}
// global value of block2 top-left = {2, 2}

var parent = new FillBlock()
{
    // ...
    Top = new Measurement() { Value = 4; Type = Measure.MeasurementType.Absolute },
    Left = new Measurement() { Value = 4; Type = Measure.MeasurementType.Absolute }
};

var child = new TextBlock()
{
    // ..
    Top = new Measurement() { Value = 2; Type = Measure.MeasurementType.Absolute },
    Left = new Measurement() { Value = 2; Type = Measure.MeasurementType.Absolute }
};

parent.PushRenderBlock(child);
renderer.PushBlockToRoot(parent);

// global value of parent top-left = {4, 4}
// global value of child top-left = {6, 6}

```

### Reading inputs

`ConsoleRenderEngine` provides the `GetInput` method to read console key-presses without blocking the console. `GetInput` returns only the first character pressed (if any) and subsequently clears the input-buffer.

```csharp
var renderer = new ConsoleRenderEngine();
var input = renderer.GetInput();
if(input != null)
{
    // handle input
}
```

## Blocks

### FillBlock

Renders a solid-colored rectangle with the associated `FillCharacter` populating each terminal-space.

```csharp
var fillBlock = new FillBlock()
{
    Id = "fill_block",
    Top = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Absolute },
    Left = new Measurement() { Value = 50, Type = Measurement.MeasurementType.Absolute },
    Width = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Absolute },
    Height = new Measurement() { Value = 10, Type = Measurement.MeasurementType.Absolute },
    TileColor = new TileColor()
    {
        ForegroundColor = purple,
        BackgroundColor = red,
    },
    FillCharacter = 'O',
}
```

### ImageBlock

Renders the selected `PNG` into the terminal. To account for the unequal dimensions of terminal-spaces, `ImageBlock` "pixels" are two terminal-spaces wide, causing the width of the `ImageBlock` to be twice the actual width of the image. 

```csharp
var imgBlock = ImageBlock.FromPNG("./path/to/img.png");
```

### TextBlock

Renders `Text` into a solid-color rectangle. `Margins` indicates the inset of the text from the boundaries of the `RenderBlock`.

```csharp
var textBlock = new TextBlock()
{
    Id = "text_block",
    Top = new Measurement() { Value = 50, Type = Measurement.MeasurementType.Percent },
    Left = new Measurement() { Value = 25, Type = Measurement.MeasurementType.Percent },
    Width = new Measurement() { Value = 50, Type = Measurement.MeasurementType.Percent },
    Height = new Measurement() { Value = 25, Type = Measurement.MeasurementType.Percent },
    TileColor = new TileColor()
    {
        ForegroundColor = new EngineColor() { R = 200, G = 200, B = 200, A= 255 },
        BackgroundColor = new EngineColor() { R = 0, G = 0, B = 0, A = 255 }
    },
    Text = "This is text",
    Margins = new Margins()
    {
        Left = new Measurement() { Value = 20, Type = Measurement.MeasurementType.Percent } , 
        Top = new Measurement() { Value = 20, Type = Measurement.MeasurementType.Percent },
        Right = new Measurement() { Value = 20, Type = Measurement.MeasurementType.Percent },
        Bottom = new Measurement() { Value = 20, Type = Measurement.MeasurementType.Percent }
    }
}
```

## Animations

Blocks can be animated using `Animation`s and can be managed with `AnimationSet`s.

```csharp
var block = new FillBlock()
{
    Id = "fill_block",
    FillCharacter = 'a'
};
var frames = new List<AnimationFrame>()
{
    new AnimationFrame()
    {
        FrameDuration = 0.1,
        Nodes = new List<AnimationNode>()
        {
            new() { Behavior => block.FillCharacter = 'b' }
        }
    },
    new AnimationFrame()
    {
        FrameDuration = 0.1,
        Nodes = new List<AnimationNode>()
        {
            new() { Behavior => block.FillCharacter = 'c' }
        }
    },
};

var animation = new Animation(frames);
var animationSet = new AnimationSet();
animationSet.Animations.Add("example", animation);
animationSet.Play("example");
animationSet.Update(0.1f);
```

## Console gameloop

```csharp
var tps = TimeSpan.TicksPerSecond;
var targetFrames = 30;
var interval = new TimeSpan(tps / targetFrames).Ticks;
var nextTick = DateTime.Now.Ticks + interval;

while (true)
{
    while (DateTime.Now.Ticks < nextTick) 
    {  
        renderer.Render();
    }
    var input = renderer.GetInput();
    // handle input
    // update state
    
    nextTick += interval;
}
```
